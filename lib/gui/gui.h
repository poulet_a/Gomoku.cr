typedef struct gui_action {
  int action;                   /* Not used yet */
  int x;
  int y;
};

typedef void* gui_game;

/* General purpose */
gui_game* gui_init();           /* Initialize the game */
int gui_clean(gui_game* game);  /* Clean the game board */

/* Game input */
struct gui_action* gui_player_action(gui_game* game);       /* Wait for an action of the GUI */
/* Game output */
int gui_game_set_status(gui_game *game, int status);        /* Check the victory */
int gui_game_set_player_turn(gui_game *game, int color);    /* Set which player */
int gui_game_message(gui_game *game, char* message, int flag); /* Send a message to the gui to a specific purpose (flag) */

/* Move tokens */
int gui_token_add(gui_game* game, int color, int x, int y); /* Add a new token */
int gui_token_remove(gui_game* game, int x, int y);         /* Remove a token */
