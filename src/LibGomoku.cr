@[Link("gui")]
lib LibGui
  struct Action
    action : LibC::Int
    x : LibC::Int
    y : LibC::Int
  end
  fun init = gui_init : Game*
  alias Game = Void*
  fun clean = gui_clean(game : Game*) : LibC::Int
  fun player_action = gui_player_action(game : Game*) : Action*
  fun game_set_status = gui_game_set_status(game : Game*, status : LibC::Int) : LibC::Int
  fun game_set_player_turn = gui_game_set_player_turn(game : Game*, color : LibC::Int) : LibC::Int
  fun game_message = gui_game_message(game : Game*, message : LibC::Char*, flag : LibC::Int) : LibC::Int
  fun token_add = gui_token_add(game : Game*, color : LibC::Int, x : LibC::Int, y : LibC::Int) : LibC::Int
  fun token_remove = gui_token_remove(game : Game*, x : LibC::Int, y : LibC::Int) : LibC::Int
end

